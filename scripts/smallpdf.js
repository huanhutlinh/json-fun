var obj = JSON.parse($response.body);

obj.data.app_metadata.expirationDate = "2099-08-12";
obj.data.app_metadata.is_in_free_trial_period = false;

$done({ body: JSON.stringify(obj) });