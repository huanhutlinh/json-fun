/** HIỂN THỊ THÔNG TIN */
const appName = decodeURIComponent($request.headers['user-agent']).split('/')[0];
$notification.post("INFO - " + appName, $request.url.replace(/^https:\/\/api\.revenuecat\.com\/v1\//, ""), "-API_KEY: " + $request.headers.authorization)

/** XỬ LÝ BODY */
var body = JSON.parse($response.body)
const urlPattern = /^https:\/\/api\.revenuecat\.com\/v1\/subscribers\/\w+$/;
if (urlPattern.test($request.url)) {
	body.subscriber.management_url = "https://apps.apple.com/account/subscriptions"
	body.subscriber.original_application_version = 113;
	body.subscriber.original_purchase_date = "2024-01-01T00:00:00Z";
}

/** XỬ LÝ HEADERS */
const params = {
	url: "https://api.revenuecat.com/v1/product_entitlement_mapping",
	timeout: 5000,
	headers: {
		'authorization': $request.headers.authorization,
		'x-platform': 'ios'
	}
};

$httpClient.get(params, function (errormsg, response, data) {
	mapping(data)
})

function mapping(data) {
	let res = JSON.parse(data)
	// console.log("res_mapping: ", res)

	if (Object.keys(res.product_entitlement_mapping).length === 0) {
		err("products or entitlements missing");
		return;
	}

	// Filter out empty entitlements
	for (let key in res.product_entitlement_mapping) {
		if (res.product_entitlement_mapping[key].entitlements.length === 0) {
			delete res.product_entitlement_mapping[key];
		}
	}
	console.log("res_mapping filter: ", res)

	if (Object.keys(res.product_entitlement_mapping).length === 0) {
		res = JSON.parse(data)
	}

	let hasLifetime = false; // Initialize flag to track lifetime product

	for (let key in res.product_entitlement_mapping) {
		let p = res.product_entitlement_mapping[key].product_identifier;
		let e = res.product_entitlement_mapping[key].entitlements;

		if (p.search(/(?!.*discount)(life(?:time)?|one(?:time)?|forever|_lt|life)/i) !== -1) {
			updateSubscription(p, e, true);
			hasLifetime = true; // Set done to true when processing lifetime product
			break;
		}
	}

	if (!hasLifetime) {
		let resVip = JSON.parse(JSON.stringify(res));

		for (let key in resVip.product_entitlement_mapping) {
			let productIdentifier = resVip.product_entitlement_mapping[key].product_identifier;
			if (productIdentifier.search(/(month|year|annual|week|_1m|_1y|_1w|trial|quarter|test)/i) !== -1) {
				delete resVip.product_entitlement_mapping[key];
			}
		}
		console.log("resVip filter: ", resVip)

		if ((Object.keys(resVip.product_entitlement_mapping).length !== 0) && (Object.keys(resVip.product_entitlement_mapping).length < 5)) {
			for (let key in resVip.product_entitlement_mapping) {
				let p = resVip.product_entitlement_mapping[key].product_identifier;
				let e = resVip.product_entitlement_mapping[key].entitlements;

				updateSubscription(p, e, true);
				break;
			}
		} else {
			resVip = JSON.parse(JSON.stringify(res));

			for (let key in resVip.product_entitlement_mapping) {
				let p = resVip.product_entitlement_mapping[key].product_identifier;
				let e = resVip.product_entitlement_mapping[key].entitlements;

				if (p.search(/(year|_1y|annual|12 ?month)/i) !== -1) {
					updateSubscription(p, e);
				}
			}
		}
	}

	console.log(body);
	$done({ body: JSON.stringify(body), status: 200 });
}

function updateSubscription(productIdentifier, entitlements, isLifetime) {
	body.subscriber.subscriptions[productIdentifier] = {
		"auto_resume_date": null,
		"billing_issues_detected_at": null,
		"expires_date": isLifetime ? null : "9999-01-01T00:00:00Z",
		"grace_period_expires_date": null,
		"is_sandbox": false,
		"original_purchase_date": "2024-01-01T00:00:00Z",
		"ownership_type": "PURCHASED",
		"period_type": "normal",
		"purchase_date": "2024-01-01T00:00:00Z",
		"refunded_at": null,
		"store": "app_store",
		"store_transaction_id": "500001676026716",
		"unsubscribe_detected_at": null
	}

	for (let i = 0; i < entitlements.length; i++) {
		body.subscriber.entitlements[entitlements[i]] = {
			"expires_date": isLifetime ? null : "9999-01-01T00:00:00Z",
			"grace_period_expires_date": null,
			"product_identifier": productIdentifier,
			"purchase_date": "2024-01-01T00:00:00Z"
		}
	}

	$notification.post("Success - " + appName, $request.url.replace(/^https:\/\/api\.revenuecat\.com\/v1\//, ""), "-ent: " + entitlements + "\n-pID: " + productIdentifier);
}
