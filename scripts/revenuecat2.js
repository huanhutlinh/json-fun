/** HIỂN THỊ THÔNG TIN */
const appName = decodeURIComponent($request.headers['user-agent']).split('/')[0];
$notification.post("INFO - " + appName, $request.url.replace(/^https:\/\/api\.revenuecat\.com\/v1\//, ""), "-API_KEY: " + $request.headers.authorization)

/** XỬ LÝ BODY */
var body = JSON.parse($response.body)
const urlPattern = /^https:\/\/api\.revenuecat\.com\/v1\/subscribers\/\w+$/;
if (urlPattern.test($request.url)) {
	body.subscriber.management_url = "https://apps.apple.com/account/subscriptions"
	body.subscriber.original_application_version = 113;
	body.subscriber.original_purchase_date = "2024-01-01T00:00:00Z";
}

/** XỬ LÝ HEADERS */
const params = {
	url: "https://api.revenuecat.com/v1/product_entitlement_mapping",
	timeout: 5000,
	headers: {
		'authorization': $request.headers.authorization,
		'x-platform': 'ios'
	}
};

$httpClient.get(params, function (errormsg, response, data) {
	mapping(data)
})

function mapping(data) {
	let res = JSON.parse(data)

	/**================================================================= */
	// xoá bớt productIdentifier không cần thiết
	for (let key in res.product_entitlement_mapping) {
		let productIdentifier = res.product_entitlement_mapping[key].product_identifier;
		if (productIdentifier.search(/(month|week|_1m|_3m|_6m|_1w|.small|.medium|.large|.huge|discount|duotone|quarter|test)/i) !== -1) {
			delete res.product_entitlement_mapping[key];
		}
	}
	console.log("\nRes Filter: \n", JSON.stringify(res))

	/**================================================================= */
	// Đối tượng để theo dõi các entitlements và danh sách product_identifier tương ứng
	var entitlementsMap = {};

	// Lặp qua các product_entitlement_mapping
	for (var productId in res.product_entitlement_mapping) {
		var productData = res.product_entitlement_mapping[productId];
		var entitlements = productData.entitlements || [''];
		var productIdentifier = productData.product_identifier;

		if (entitlements.length === 0) {
			(entitlementsMap[''] = entitlementsMap[''] || []).push(productIdentifier);
		} else {
			entitlements.forEach(function (entitlement) {
				(entitlementsMap[entitlement] = entitlementsMap[entitlement] || []).push(productIdentifier);
			});
		}
	}

	// Hiển thị kết quả
	console.log("\nGộp các product_identifier có cùng entitlements: \n");
	console.log(JSON.stringify(entitlementsMap));

	/**================================================================= */
	for (let key in entitlementsMap) {
		let e = key;
		let p = entitlementsMap[key];
		var hasLifetimeProduct = false; // Biến để kiểm tra xem có sản phẩm lifetime hay không

		// Lặp qua danh sách sản phẩm để kiểm tra
		for (var j = 0; j < p.length; j++) {
			if (p[j].search(/(?!.*discount)(life(?:time)?|one(?:time)?|forever|_lt|life)/i) !== -1) {
				updateSubscription([p[j]], e, true);
				hasLifetimeProduct = true; // Đánh dấu rằng có sản phẩm lifetime
				break;
			}
		}

		// Nếu không có sản phẩm lifetime, cập nhật tất cả các sản phẩm
		if (!hasLifetimeProduct) {
			updateSubscription(p, e);
		}
	}

	// If no lifetime product found, check for VIP products
	if (!hasLifetimeProduct) {
		let resVip = JSON.parse(JSON.stringify(res));

		for (let key in resVip.product_entitlement_mapping) {
			let productIdentifier = resVip.product_entitlement_mapping[key].product_identifier;
			if (productIdentifier.search(/(year|annual|_1y|trial|earlyvip)/i) !== -1) {
				delete resVip.product_entitlement_mapping[key];
			}
		}

		// Process VIP products
		if ((Object.keys(resVip.product_entitlement_mapping).length !== 0) && (Object.keys(resVip.product_entitlement_mapping).length < 5)) {
			for (let key in resVip.product_entitlement_mapping) {
				let p = resVip.product_entitlement_mapping[key].product_identifier;
				let e = resVip.product_entitlement_mapping[key].entitlements;
				if (Array.isArray(e)) {
					// If 'entitlements' is an array, update subscriptions for each entitlement
					for (let i = 0; i < e.length; i++) {
						updateSubscription([p], e[i], true);
					}
				} else {
					// If 'entitlements' is not an array, update subscription normally
					updateSubscription([p], e, true);
				}
				break; // Exiting loop after one iteration
			}
		}
	}

	/**================================================================= */
	console.log("\nbody: \n", JSON.stringify(body))
	$done({ body: JSON.stringify(body) })
}

/**================================================================= */
function updateSubscription(productIdentifier, entitlements, isLifetime) {
	for (let i = 0; i < productIdentifier.length; i++) {
		body.subscriber.subscriptions[productIdentifier[i]] = {
			// "auto_resume_date": null,
			// "billing_issues_detected_at": null,
			"expires_date": isLifetime ? null : "9999-01-01T00:00:00Z",
			// "grace_period_expires_date": null,
			// "is_sandbox": false,
			"original_purchase_date": "2024-01-01T00:00:00Z",
			"ownership_type": "PURCHASED",
			"period_type": "normal",
			"purchase_date": "2024-01-01T00:00:00Z",
			// "refunded_at": null,
			// "store": "app_store",
			// "store_transaction_id": "500001676026716",
			// "unsubscribe_detected_at": null
		}

		body.subscriber.entitlements[entitlements] = {
			"expires_date": isLifetime ? null : "9999-01-01T00:00:00Z",
			// "grace_period_expires_date": null,
			"product_identifier": productIdentifier[i],
			"purchase_date": "2024-01-01T00:00:00Z"
		}
	}
	$notification.post("Success - " + appName, $request.url.replace(/^https:\/\/api\.revenuecat\.com\/v1\//, ""), "-ent: " + entitlements + "\n-pID: " + productIdentifier);
}