let obj = {
	"request_date": "2024-04-19T12:00:18Z",
	"request_date_ms": 1713528018686,
	"subscriber": {
		"entitlements": {
			"Advanced": {
				"expires_date": "8888-04-04T17:16:46Z",
				"grace_period_expires_date": null,
				"product_identifier": "com.palligroup.gpt3.weeklyyy",
				"purchase_date": "2024-04-01T17:16:46Z"
			}
		},
		"first_seen": "2024-04-01T17:14:07Z",
		"last_seen": "2024-04-19T10:36:43Z",
		"management_url": null,
		"non_subscriptions": {},
		"original_app_user_id": "$RCAnonymousID:af4ab9f2bc184d0fbbe457bb85ef3ee0",
		"original_application_version": "1.1",
		"original_purchase_date": "2024-04-01T17:12:11Z",
		"other_purchases": {},
		"subscriptions": {
			"com.palligroup.gpt3.weeklyyy": {
				"auto_resume_date": null,
				"billing_issues_detected_at": null,
				"expires_date": "8888-04-04T17:16:46Z",
				"grace_period_expires_date": null,
				"is_sandbox": false,
				"original_purchase_date": "2024-04-01T17:16:48Z",
				"ownership_type": "PURCHASED",
				"period_type": "normal",
				"purchase_date": "2024-04-01T17:16:46Z",
				"refunded_at": null,
				"store": "app_store",
				"store_transaction_id": "500001681362038",
				"unsubscribe_detected_at": "2024-04-01T17:23:16Z"
			}
		}
	}
}

$done({ body: JSON.stringify(obj) })