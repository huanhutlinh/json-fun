re('("expires_date"):"\\w{4}@("expires_date_pst"):"\\w{4}@("expires_date_ms"):"\\w+"@("is_trial_period"):"\\w+"', '$1:"2099@$1:"2099@$1:"4096019658000"@$1:"false"');
function re() {
	var body = $response.body;
	if (!body) {
		$done({});
	}
	if (arguments[0].includes("@")) {
		var regs = arguments[0].split("@");
		var strs = arguments[1].split("@");
		for (i = 0; i < regs.length; i++) {
			var reg = new RegExp(regs[i], "g");
			body = body.replace(reg, strs[i]);
		}
	}
	else {
		var reg = new RegExp(arguments[0], "g");
		body = body.replace(reg, arguments[1]);
	}
	$done({ body });
}


/*
let body = $response.body;
var obj = JSON.parse(body);

if (body.indexOf("expires") != -1) {
	obj["receipt"]["in_app"][0]["expires_date"] = "2099-10-19 05:14:18 Etc/GMT";
	obj["receipt"]["in_app"][0]["expires_date_pst"] = "2099-10-18 22:14:18 America/Los_Angeles";
	obj["receipt"]["in_app"][0]["expires_date_ms"] = "4096019658000";
	obj["latest_receipt_info"][0]["expires_date"] = "2099-10-19 05:14:18 Etc/GMT";
	obj["latest_receipt_info"][0]["expires_date_pst"] = "2099-10-18 22:14:18 America/Los_Angeles";
	obj["latest_receipt_info"][0]["expires_date_ms"] = "4096019658000";
}

$done({ body: JSON.stringify(obj) });
*/